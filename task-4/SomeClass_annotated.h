#pragma once

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <vector>
#include <limits>

#include <stdint.h> 

/*************** Random number generation *******************/
// Taken from OpenCV
typedef uint64_t CvRNG;
#define CV_RNG_COEFF 4164903690U

inline CvRNG cvRNG( int64_t seed=-1)
{
	CvRNG rng = seed ? (uint64_t)seed : (uint64_t)(int64_t)-1;
	return rng;
}

/* Return random 32-bit unsigned integer: */
inline unsigned cvRandInt( CvRNG* rng )
{
	uint64_t temp = *rng;
	temp = (uint64_t)(unsigned)temp*CV_RNG_COEFF + (temp >> 32);
	*rng = temp;
	return (unsigned)temp;
}

/* Returns random floating-point number between 0 and 1: */
inline double cvRandReal( CvRNG* rng )
{
	return cvRandInt(rng)*2.3283064365386962890625e-10 /* 2^-32 */;
}
/**************************************************************/

template<class T>
class SomeThing
{
	public:
		SomeThing(int _z) : z(_z){}
		
		size_t operator()(T x) const
		{
			union { T a; size_t b; } u;
			u.b = 0;
			u.a = x;
			
			CvRNG rng1 = cvRNG(z*(z+1) + u.b);  //seed rng with object x and z
			return (size_t)( cvRandReal(&rng1)*(double)(UINT32_MAX) );  //return random number
		}
		
	private:
		int  z;
};

//bloom filter
template<class T, class H=SomeThing<T> >
class SomeClass
{
	public:
		SomeClass(int nb, int nh);
		
		~SomeClass();
		
		void c();  //reset arr_ and c_
		
		void f(const T& x);  //populate arr_
		
		bool g(const T& x) const;  //runs the same computation as f() and compares with arr_ (that was populated using f()).  If computation is equivalent return true.
		
		bool h()  const;  //true if arr_ hasn't been populated yet
		
		size_t u() const;  //accessor function for c_
		
	private:
		unsigned char*    arr_;  //all elements of array are 8 zero bits with a single 1 bit. eg. [00000001, 01000000, 00000100 ...]
		int               l_;
		int               c_;
		size_t            b_;
		std::vector<H>    ff;
};

