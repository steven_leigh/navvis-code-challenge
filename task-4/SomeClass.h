#pragma once

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <vector>
#include <limits>

#include <stdint.h> 

/*************** Random number generation *******************/
// Taken from OpenCV
typedef uint64_t CvRNG;
#define CV_RNG_COEFF 4164903690U

inline CvRNG cvRNG( int64_t seed=-1)
{
	CvRNG rng = seed ? (uint64_t)seed : (uint64_t)(int64_t)-1;
	return rng;
}

/* Return random 32-bit unsigned integer: */
inline unsigned cvRandInt( CvRNG* rng )
{
	uint64_t temp = *rng;
	temp = (uint64_t)(unsigned)temp*CV_RNG_COEFF + (temp >> 32);
	*rng = temp;
	return (unsigned)temp;
}

/* Returns random floating-point number between 0 and 1: */
inline double cvRandReal( CvRNG* rng )
{
	return cvRandInt(rng)*2.3283064365386962890625e-10 /* 2^-32 */;
}
/**************************************************************/

template<class T>
class SomeThing
{
	public:
		SomeThing(int _z) : z(_z){}
		
		size_t operator()(T x) const
		{
			union { T a; size_t b; } u;
			u.b = 0;
			u.a = x;
			
			CvRNG rng1 = cvRNG(z*(z+1) + u.b);
			return (size_t)( cvRandReal(&rng1)*(double)(UINT32_MAX) );
		}
		
	private:
		int  z;
};

template<class T, class H=SomeThing<T> >
class SomeClass
{
	public:
		SomeClass(int nb, int nh);
		
		~SomeClass();
		
		void c();
		
		void f(const T& x);
		
		bool g(const T& x) const;
		
		bool h()  const;
		
		size_t u() const;
		
	private:
		unsigned char*    arr_;
		int               l_;
		int               c_;
		size_t            b_;
		std::vector<H>    ff;
};

