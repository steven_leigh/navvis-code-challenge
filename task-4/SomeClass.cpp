#include "SomeClass.h"

#include <cstring>

template<class T, class H>
SomeClass<T, H>::SomeClass(int nb, int nh)
  : l_(0), c_(0), arr_(0)
{	
	b_   = nb;
	l_   = nb / 8 + 1;
	arr_ = new unsigned char[l_];
	c();
	
	for(int i=1; i<=nh; i++)
		ff.push_back( H(i) );
}

template<class T, class H>
SomeClass<T, H>::~SomeClass()
{
	if( arr_ )
		delete[] arr_;
}

template<class T, class H>
void SomeClass<T, H>::c()
{
	memset(arr_, 0, sizeof(unsigned char)*l_);
	c_ = 0;
}

template<class T, class H>
void SomeClass<T, H>::f(const T& x)
{
	for(size_t j=0; j<ff.size(); j++){
		size_t key = ff[j](x) % b_;
		arr_[ key / 8 ] |= (unsigned char)(1 << (key % 8));
	}
	c_++;
}

template<class T, class H>
bool SomeClass<T, H>::g(const T& x) const
{
	size_t z = 0;
	for(size_t j=0; j<ff.size(); j++){
		size_t key  = ff[j](x) % b_;
		z += (arr_[ key / 8 ] & (unsigned char)(1 << (key % 8)) ) > 0 ? 1 : 0;
	}
	return ( z == ff.size() );
}

template<class T, class H>
bool SomeClass<T, H>::h()  const
{
	return (u() == 0);
}

template<class T, class H>
size_t SomeClass<T, H>::u() const
{
	return c_;
}

template class SomeThing<int>;
template class SomeClass<int>;
// ...
