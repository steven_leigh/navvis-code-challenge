//============================================================================
// Name        : task_3.cpp
// Author      : Steven Leigh
// Version     :
// Copyright   :
// Description : NavVis coding challenge Task 3.  Sorting one list based on another.
//============================================================================

#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>
using namespace std;


//Wrap the compare function passed to std::sort.  The user only supplies
//a compare function for the second container as should be expected, but
//the actual list being sorted is a list of pairs of elements from both
//lists.  This wrapper dissects the pair structure so the original compare
//function can be used.
template<class T, class U>
class compare_wrapper{
	public:
		//pass in original compare function
		compare_wrapper(bool (*compare_func_in)(U, U)){
			compare_func = compare_func_in;
		}
		//dissect pair and pass second element to original compare function
		bool operator()(pair<T,U>* a, pair<T,U>* b){
			return compare_func(a->second, b->second);
		}
	private:
		bool (*compare_func)(U, U);  //original compare function for second container
};

//Accepts two vector pointers of any class and a compare function for
//elements of the second vector.  The first vector is replaced with its sorted version.
template <class T, class U>
void two_list_sort(vector<T>* vect_1, vector<U>* vect_2, bool (*compare_func)(U, U)){
	//copy both lists into a single list of pairs
	vector< pair<T,U>* > vect_both;
	for (int i=0; i<vect_1->size(); i++){
		pair<T,U>* p = new pair<T,U>(vect_1->at(i), vect_2->at(i));
		vect_both.push_back(p);
	}

	//sort pairs using the second element in each pair
	compare_wrapper<T, U> cw(compare_func);
	sort(vect_both.begin(), vect_both.end(), cw);

	//move sorted elements back into vect_1
	for (int i=0; i<vect_1->size(); i++){
		vect_1->at(i) = vect_both[i]->first;
	}

	//clean up
	for (int i=0; i<vect_both.size(); i++){
		delete vect_both.back();
		vect_both.pop_back();
	}
	return;
}



bool comp(int a, int b){
	return a<b;
}


int main() {
	///int list_1[] = {1,2,3,4,5,6,7,8};
	//vector<char>* vect_1 = new vector<int>(list_1, list_1+sizeof(list_1)/sizeof(int));
	char list_1[] = {'a','b','c','d','e','f','g','h'};
	vector<char>* vect_1 = new vector<char>(list_1, list_1+sizeof(list_1)/sizeof(char));
	int list_2[] = {8,7,6,5,4,3,2,1};
	vector<int>* vect_2 = new vector<int>(list_2, list_2+sizeof(list_2)/sizeof(int));

	cout<<"list 1 pre sort: ";
	for (int i=0; i<vect_1->size(); i++){
		cout << vect_1->at(i) << ',';
	}

	two_list_sort(vect_1, vect_2, comp);

	cout<<"\nlist 1 post sort: ";
	for (int i=0; i<vect_1->size(); i++){
		cout << vect_1->at(i) << ',';
	}
	cout << '\n';

	return 0;
}








