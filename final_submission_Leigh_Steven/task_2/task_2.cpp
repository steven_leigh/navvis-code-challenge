//============================================================================
// Name        : task_2.cpp
// Author      : Steven Leigh
// Version     :
// Copyright   :
// Description : NavVis coding challenge Task 2.  CSV parsing and simple processing.
// Task assumptions: 	-input CSV files are well formatted
//						-matrix is always rectangular
//						-matrix values are always in the valid range of float32
//						-interpolation from 4-connect neighbours
//						-user inputs correct csv file paths
//============================================================================

#include <iostream>
#include <fstream>
#include <string>
#include <list>
#include <stdlib.h>
using namespace std;

char delim;  // csv file delimiter

// read all lines from the csv file
list<string>* read_all(const char* fname){
	list<string>* out = new list<string>();
	string line;
	ifstream f(fname);
	if (!f.is_open()){
		cout<<"Error: Can't read input csv file.\n";
		exit(-1);
	}
	while (getline(f, line)){
		out->push_back(line);
	}
	f.close();
	return out;
}

// Parse the list of strings into the matrix array.
// The strings are parsed destructively.
// Allocates matrix.  Caller must destroy matrix.
// Populates height and width with matrix dimensions.
void parse(list<string>* text, double** &mat, int &height, int &width){
	// detect delimiter
	if (text->front().find(',') == string::npos){
		delim = ' ';
	}else{
		delim = ',';
	}

	// find matrix height
	height = text->size();

	// find matrix width
	string line = text->front();
	width = 1;  //width is number of delimiters plus one
	for (int i = 0; i<line.length(); i++){
		if (line[i]==delim)
			width++;
	}

	// allocate matrix
	mat = new double*[height];
	for(int i = 0; i<height; i++)
		mat[i] = new double[width];


	// go through text and populate matrix
	for (int row = 0; row<height; row++){
		string line = text->front();
		text->pop_front();
		for (int col = 0; col<width; col++){
			size_t pos = line.find(delim);
			if (pos == string::npos){  //no delimiter after last element in row
				mat[row][col] = atof(line.c_str());
			}else{
				mat[row][col] = atof(line.substr(0,pos).c_str());
				line = line.substr(pos+1, line.length());
			}
		}
	}
	return;
}

// Interpolate the value from neighbours at a given row and column.
// Assume 4 connect neighbours for interpolation.
double interpolate(double** mat, int height, int width, int row, int col){
	int row_delta[4] = {1,0,0,-1};
	int col_delta[4] = {0,1,-1,0};
	double divisor = 0.0;
	double sum = 0.0;
	for (int i = 0; i<4; i++){  //iterate through all valid neighbours and accumulate
		int r = row + row_delta[i];
		int c = col + col_delta[i];
		if (r<0 or r>=height or c<0 or c>=width)
			continue;
		sum+=mat[r][c];
		divisor+=1.0;
	}
	return sum/divisor;
}

// Identify the zero values in matrix and replace with interpolated value
void replace_bad(double** &mat, int height, int width){
	for (int row = 0; row<height; row++){
		for (int col = 0; col<width; col++){
			if (mat[row][col] == 0.0)
				mat[row][col] = interpolate(mat, height, width, row, col);
		}
	}
	return;
}

// Write the matrix to a csv file
void write_csv(const char* fname, double** mat, int height, int width){
	ofstream f(fname);
	if (!f.is_open()){
		cout<<"Error: Can't write to output csv file.\n";
		exit(-1);
	}
	for (int row = 0; row<height; row++){
		for (int col = 0; col<width; col++){
			f << mat[row][col];
			if (col<(width-1))  //don't put delimiter after last element in row
				f << delim;
		}
		f << '\n';
	}
	f.close();
}

// Program accepts two file paths as arguments: <input.csv> <output.csv>
int main(int argc, char *argv[]) {
	list<string>* csv_contents;		// stores contents from csv file
	double** mat;					// 2d matrix
	int height, width;				// dimensions of matrix

	//csv_contents = read_all("simple_matrix.csv");
	//csv_contents = read_all("simple_replace.csv");
	//csv_contents = read_all("tricky_replace.csv");
	csv_contents = read_all(argv[1]);

	// parse csv file
	parse(csv_contents, mat, height, width);

	// replace bad matrix values
	replace_bad(mat, height, width);

	// write out to another csv file
	write_csv(argv[2], mat, height, width);

	// clean up
	for(int i = 0; i<height; i++)
			delete mat[i];
	delete mat;

	return 0;
}



























